Clone Dockerfile

    git clone git@gitlab.com:chunlee1991/ethereum_dockerfile.git


Build Image

    cd ethereum_dockerfile
    docker build -t ethereum:mainnet .



Build Container

    docker run -v /home/.logs:/root/.logs -d -p :22 -p :30303 -p :8545 --name ethereum ethereum:mainnet /root/start.sh


Run Ethereum Mainnet Node

``` shell
   screen -S mainnet
   bash
   /root/go-ethereum/build/bin/geth --syncmode "light" --nousb --rpc --rpcapi="db,eth,net,web3,personal" console
```
   
[**Geth Reference**](https://github.com/ethereum/go-ethereum/wiki/Command-Line-Options)
